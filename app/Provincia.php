<?php

namespace sisContratista;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $table='provincias';
    protected $primaryKey='idprovincias';


    public $timestamps = false;

    protected $fillable=[
        'descripcion',
        'idpaises'
    ];

    protected $guarded=[
        
    ];

    public function pais(){
        return $this->belongsto(Pais::class);
    }

}
