<?php

namespace sisContratista;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table='paises';
    protected $primaryKey='idpaises';


    public $timestamps = false;

    protected $fillable=[
        'descripcion'
    ];

    protected $guarded=[
        
    ];

    public function provincias(){
        return $this->hasmany(Provincias::class);
    }

}
