<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/home', function () {
    return view('layouts/admin');
});

Route::resource('empleado/pais','PaisController');

Route::resource('empleado/provincia','ProvinciaController');

Route::auth();

Route::get('/', 'HomeController@index');
