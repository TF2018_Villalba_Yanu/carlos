<?php

namespace sisContratista\Http\Controllers;

use Illuminate\Http\Request;

use sisContratista\Http\Requests;
use sisContratista\Provincia;
use Illuminate\Support\Facades\Redirect;
use sisContratista\Http\Requests\ProvinciaFormRequest;
use DB;

class ProvinciaController extends Controller
{   

    public function __construct()
    {
        
    }


    public function index(Request $request)
    {
        if($request){
            //filtro de busqueda
            $query=trim($request->get('searchText'));
            $provincias=DB::table('provincias as pr')
                ->join('paises as pa','pr.idpaises','=','pa.idpaises')
                ->select('pr.descripcion','pa.descripcion as categoria')
                ->where('pr.descripcion','LIKE','%'.$query.'%')
                ->orderBy('pr.descripcion','asc')
                ->paginate(7);
            return view('empleado.provincia.index',["provincias"=>$provincias,"searchText"=>$query]);
        }
    }


    public function create()
    {
        $paises=DB::table('paises')->get();
        return view("empleado.provincia.create");
    }


    public function store(ProvinciaFormRequest $request)
    {
        $provincias=new Provincia;
        $provincias->descripcion=$request->get('descripcion');
        $provincias->save();
        return Redirect::to('empleado/provincia');
    }


    public function show($id)
    {
        $provincias=Provincia::findOrFail($id);
        $provincias=DB::table('paises')->get();
        return view('empleado.provincia.show',["provincias"=>$provincias]);

    }

 
    public function edit($id)
    {
        return view('empleado.pais.edit',["paises"=>Pais::findOrFail($id)]);
    }

 
    public function update(ArticuloFormRequest $request, $id)
    {
        $provincias=Provincia::findOrFail($id);
        $provincias->descripcion=$request->get('descripcion');
        $provincias->update();
        return Redirect::to('empleado/provincia');
    }


    public function destroy($id)
    {
        $provincias=Provincia::findOrFail($id);
        Provincia::destroy($id);
        return Redirect::to('empleado/provincia');
    }
}

