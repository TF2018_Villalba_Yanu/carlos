<?php

namespace sisContratista\Http\Controllers;

use Illuminate\Http\Request;

use sisContratista\Http\Requests;
use sisContratista\Pais;
use Illuminate\Support\Facades\Redirect;
use sisContratista\Http\Requests\PaisFormRequest;
use DB;

class PaisController extends Controller
{   

    public function __construct()
    {
        
    }


    public function index(Request $request)
    {
        if($request){
            //filtro de busqueda
            $query=trim($request->get('searchText'));
            $paises=DB::table('paises')
                ->where('descripcion','LIKE','%'.$query.'%')
                ->orderBy('descripcion','asc')
                ->paginate(7);
            return view('empleado.pais.index',["paises"=>$paises,"searchText"=>$query]);
        }
    }


    public function create()
    {
        return view("empleado.pais.create");
    }


    public function store(PaisFormRequest $request)
    {
        $paises=new Pais;
        $paises->descripcion=$request->get('descripcion');
        $paises->save();
        return Redirect::to('empleado/pais');
    }


    public function show($id)
    {
        return view('empleado.pais.show',["paises"=>Pais::findOrFail($id)]);

    }

 
    public function edit($id)
    {
        return view('empleado.pais.edit',["paises"=>Pais::findOrFail($id)]);
    }

 
    public function update(PaisFormRequest $request, $id)
    {
        $paises=Pais::findOrFail($id);
        $paises->descripcion=$request->get('descripcion');
        $paises->update();
        return Redirect::to('empleado/pais');
    }


    public function destroy(Pais $paises)
    {
        if(count($paises->departamentos)){
            return 'Pais tiene asociado Provincias';
        }

        Pais::destroy($paises->$id);
        return 'Borrado satisfactoriamente';
    }
}
