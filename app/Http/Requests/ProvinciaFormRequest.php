<?php

namespace sisContratista\Http\Requests;

use sisContratista\Http\Requests\Request;

class ProvinciaFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion'=>'required|max:45',
            'idpaises'=>'required'
        ];
    }
}
