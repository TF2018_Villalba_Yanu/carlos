@extends('layouts.admin')


@section('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12">
            <h3>Listado de Paises<a href="pais/create"><button class="btn btn-success"> Nuevo</button></a></h3>
            @include('empleado.pais.search')
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                        <th>Descripcion</th>
                        <th>Opciones</th>
                    </thead>
                    @foreach ($paises as $item)
                    <tr>
                        <td>{{$item->descripcion}}</td>
                        <td>
                            <a href="{{URL::action('PaisController@edit',$item->idpaises)}}"><button class="btn btn-info">Editar</button></a>
                            <a href="" data-target="#modal-delete-{{$item->idpaises}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
                        </td>
                    </tr>
                    @include('empleado.pais.modal')        
                    @endforeach
                </table>
            </div>
            {{$paises->render()}}
        </div>
    </div>
@endsection