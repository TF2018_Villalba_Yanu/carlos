@extends('layouts.admin')


@section('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12">
            <h3>Listado de Provincias<a href="provincia/create"><button class="btn btn-success"> Nuevo</button></a></h3>
            @include('empleado.provincia.search')
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-condensed table-hover">
                    <thead>
                        <th>Descripcion</th>
                        <th>Categoria</th>
                        <th>Opciones</th>
                    </thead>
                    @foreach ($provincias as $item)
                    <tr>
                        <td>{{$item->descripcion}}</td>
                        <td>{{$item->categoria}}</td>
                        <td>
                            <a href="{{URL::action('ProvinciaController@edit',$item->idprovincias)}}"><button class="btn btn-info">Editar</button></a>
                            <a href="" data-target="#modal-delete-{{$item->idprovincias}}" data-toggle="modal"><button class="btn btn-danger">Eliminar</button></a>
                        </td>
                    </tr>
                    @include('empleado.provincia.modal')        
                    @endforeach
                </table>
            </div>
            {{$provincias->render()}}
        </div>
    </div>
@endsection