@extends('layouts.admin')
@section('contenido')
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <h3>Nueva provincia</h3>
            @if (count($errors)>0)
            <div class="alert alert-danger">
                <ul>
                @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
                </ul>
            </div>
            @endif 
        </div>      
    </div>   
        {!!Form::open(array(
            'url'=>'empleado/provincia',
            'method'=>'POST',
            'autocomplete'=>'off'
            ))!!}
        {{Form::token()}}
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="descripcion">Descripcion</label>
                    <input type="text" 
                    name="descripcion"
                    required value="{{old('descripcion')}}"
                    class="form-control"
                    placeholder="Nombre...">
                </div>
            </div>

            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="form-group">
                    <label for="descripcion">Paises</label>
                    <select name="idpaises" class="form-control">
                        @foreach ($paises as $item)
                            <option value="{{$item->idpaises}}">{{$item->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                <div class="form-group">
                    <button class="btn btn-primary"
                         type="submit">Guardar</button>
                    <button class="btn btn-danger"
                         type="reset">Cancelar</button>
                </div>
            </div>
        </div>
    
{!!Form::close()!!}
  
@endsection